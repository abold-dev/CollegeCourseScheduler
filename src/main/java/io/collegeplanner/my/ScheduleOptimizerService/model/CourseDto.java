package io.collegeplanner.my.ScheduleOptimizerService.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourseDto {
    private String id;
    private String name;
    private String title;
}
